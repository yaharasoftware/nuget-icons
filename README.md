# Nuget Icons #

This is a public repository to be able to host icons and (if applicable) license files for Nuget packages using the Bitbucket raw file access via the following URL pattern:

```
https://bitbucket.org/<account>/<repo-name>/raw/<commit-sha-or-HEAD>/<filename>
```

This is needed because Nuget packages need the icon and/or license to be publicly available from a URL defined in the `.nuspec` file in the current iteration.  There is a [GitHub issue](https://github.com/NuGet/Home/issues/352) tracking a fix to this.  At the time of this writing, the spec was still under review.

# Directions #

Create a separate top-level directory for each package, and place your icon in there.  DO NOT place anything else in there, as this is publicly viewable.
